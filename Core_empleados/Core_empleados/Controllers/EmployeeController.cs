﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_empleados.Models;
using Microsoft.AspNetCore.Mvc;

namespace Core_empleados.Controllers
{
    public class EmployeeController : Controller
    {

        private readonly ApplicationDbContext _db;

        public EmployeeController (ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {

            var displaydata = _db.Employee.ToList();
            return View(displaydata);
        }
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Employee nEMP)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nEMP);
                      await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(nEMP);

        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id==null)
            {
                return RedirectToAction("Index");

            }
            var getEmpDetail= await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]

        public async Task<IActionResult> Edit(Employee oldEmp)

        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(oldEmp);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetails = await _db.Employee.FindAsync(id);
            return View(getEmpDetails);
        }

        [HttpPost]

        public async Task<IActionResult> Delete(int id)

        {
            var getEmpDetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpDetail);
                await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }
    }
}
