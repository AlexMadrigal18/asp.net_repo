﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_Cliente.Models
{
    public class ClienteValidation
    {
        [MetadataType(typeof(Cliente.MetaData))]
        public partial class Cliente
        {
            sealed class MetaData
            {
                [Key]
                public int Cid;

                [Required(ErrorMessage = "Ingresa Nombre valido")]
                public string Cname;

                [Required(ErrorMessage = "Ingresa Apellido valido")]
                public string Capell;

                [Required(ErrorMessage = "Ingresa email valido")]
                public string Cmail;

                [Required]
                [Range(1500, 100000, ErrorMessage = "Ingresa Edad valida")]
                public Nullable<int> Csaldo;
    }
        }
    }
}