﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_Employee.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData
        {
            [Key]
            public int Empid;

            [Required(ErrorMessage = "Ingresa el nombre del empleado")]
            public string Empname;

            [Required(ErrorMessage = "Ingresa email valido")]
            public string Email;

            [Required]
            [Range(18,50,ErrorMessage = "Ingresa Edad valida")]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa un salario valido")]
            public Nullable<int> Salary;
        }
    }
}