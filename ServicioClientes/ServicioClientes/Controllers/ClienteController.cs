﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServicioClientes.Models;

namespace ServicioClientes.Controllers
{
    public class ClienteController : Controller
    {

        private readonly ApplicationDbContext _db;

        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {

            var displaydata = _db.Cliente.ToList();
            return View(displaydata);
        }
        [HttpGet]

        public async Task<IActionResult> Index(String cSearch)

        {
            ViewData["GetEmployeesDetails"] = cSearch;
            var empquery = from x in _db.Cliente select x;
            if (!String.IsNullOrEmpty(cSearch))
            {
                empquery = empquery.Where(x => x.Cname.Contains(cSearch) || x.Cmail.Contains(cSearch));

            }
            return View(await empquery.AsNoTracking().ToListAsync());

        }
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Cliente nCLT)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nCLT);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(nCLT);

        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getCDetail = await _db.Cliente.FindAsync(id);
            return View(getCDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getCDetail = await _db.Cliente.FindAsync(id);
            return View(getCDetail);
        }

        [HttpPost]

        public async Task<IActionResult> Edit(Cliente vCLT)

        {
            if (ModelState.IsValid)
            {
                _db.Update(vCLT);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(vCLT);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCDetails = await _db.Cliente.FindAsync(id);
            return View(getCDetails);
        }

        [HttpPost]

        public async Task<IActionResult> Delete(int id)

        {
            var getCDetail = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(getCDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

    }
}
