﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ServicioClientes.Models
{
    public class Cliente
    {
        [Key]
        public int Cid { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del cliente")]
        [Display(Name = "Nombre del cliente")]
        public string Cname { get; set; }

        [Required(ErrorMessage = "Ingresa el apellido del cliente")]
        [Display(Name = "Apellido del cliente")]
        public string Capell { get; set; }

        [Required(ErrorMessage = "Ingresa el email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Ingresa un email valido")]
        public string Cmail { get; set; }

        [Required(ErrorMessage = "Enter the employee salary")]
        [Display(Name = "Saldo")]
        public int Csaldo { get; set; }
    }
}
